package org.lilei;

/**
 * @author LiLei
 */
@RpcService(IHelloService.class)
public class IHelloServiceImpl implements IHelloService {
    @Override
    public String sayHello(String context) {
        System.out.println("request in :" + context);
        return "server: " + context + "   hello ";
    }
}
