package org.lilei;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author lilei
 * @date 2020-04-03
 */
@Configuration
@ComponentScan(basePackages = "org.lilei")
public class SpringConfig {
    @Bean(name = "rpcServer")
    public RpcServer rpcServer() {
        return new RpcServer(8080);
    }
}

