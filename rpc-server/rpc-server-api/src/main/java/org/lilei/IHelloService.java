package org.lilei;

/**
 * @author LiLei
 */
@RpcClient
public interface IHelloService {
    String sayHello(String context);
}
