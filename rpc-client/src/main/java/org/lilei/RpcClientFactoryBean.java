package org.lilei;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author lilei
 * @date 2020-04-05
 */
public class RpcClientFactoryBean<T> implements FactoryBean<T> {
    private final Class<T> interfaceCls;
    private final String host;
    private final int port;

    public RpcClientFactoryBean(Class<T> interfaceCls, String host, int port) {
        this.interfaceCls = interfaceCls;
        this.host = host;
        this.port = port;
    }

    @Override
    public T getObject() throws Exception {
        return RpcProxyClient.proxyClient(interfaceCls, host, port);
    }

    @Override
    public Class<?> getObjectType() {
        return interfaceCls;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }
}
