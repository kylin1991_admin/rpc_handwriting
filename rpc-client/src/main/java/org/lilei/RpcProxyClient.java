package org.lilei;

import java.lang.reflect.Proxy;

/**
 * @author lilei
 * @date 2020-04-03
 */
public class RpcProxyClient {
    public static <T> T proxyClient(Class<?> interfacesCls, String host, int port) {
        return (T) Proxy.newProxyInstance(
                interfacesCls.getClassLoader(),
                new Class<?>[]{interfacesCls},
                new RemoteInvocationHandler(host,port));
    }
}
