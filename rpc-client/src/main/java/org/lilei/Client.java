package org.lilei;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author lilei
 * @date 2020-04-03
 */
@ComponentScan("org.lilei")
public class Client {

    public static void main(String[] args) {
        AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(Client.class);
        IHelloService helloService = applicationContext.getBean(IHelloService.class);
        String result = helloService.sayHello("lilei");
        System.out.println(result);
    }
}
